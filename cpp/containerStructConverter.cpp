/**
 * Sample application to demonstrate conversion 
 * between containers of two different structures.
 * 
 * Uses std::transform to tranverse the original container
 * and a custom converter object.
 * 
 * Compile with
 * $ g++ containerStructConverter.cpp -o containerStructConverter
 */

#include <string>
#include <vector>
#include <list>
#include <boost/lexical_cast.hpp>

using namespace std;

struct Original
{
	Original() : value("") {};
	Original(std::string s) : value(s) {};
	std::string value;
};

struct Transformed
{
	Transformed() : value(0)
	{
	}
	int value;
};

template <typename O, typename T, typename C>
T convert(const O& original, C& converter)
{
	T res = converter(original);
	return res;
}

struct ObjectConverter
{
	Transformed operator()(const Original& original)
	{
		using boost::lexical_cast;
		using boost::bad_lexical_cast;
		Transformed t;
		try
		{
			t.value = lexical_cast<int>(original.value);
		}
		catch(bad_lexical_cast &e)
		{
			t.value = 0;
		}
		return t;
	}
} converter;

// We implement this function as a template, because 
// both Original and Transformed structures have 
// a "value" attribute
template <typename T>
void printTransformedData(const T &t)
{
	cout << "\t" << t.value << endl;
}

int main(int argc, char const *argv[])
{
	vector<Original> vStrings;
	list<Transformed> lInts;

	// Populates original container
	vStrings.push_back(Original("23"));
	vStrings.push_back(Original("4"));
	vStrings.push_back(Original("Some text"));
	vStrings.push_back(Original("124"));
	vStrings.push_back(Original("1"));

	// Allocates enough space on destination container
	lInts.resize(vStrings.size());

	transform(vStrings.begin(), 
		vStrings.end(),
		lInts.begin(),
		converter);

	// Display the results
	cout << "Original vector:" << endl;
	for_each(vStrings.begin(),
		vStrings.end(),
		printTransformedData<Original>);

	cout << "Transformed list:" << endl;
	for_each(lInts.begin(),
		lInts.end(),
		printTransformedData<Transformed>);
	return 0;
}
