/*
* Sample application to demonstrate the steps needed to reschedule
* a boost::deadline_timer.
*
* Compile with
* $ g++ deadline_timerTest.cpp -o deadline_timerTest -lboost_thread -lboost_system
*/

#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

using namespace std;

/**
 * @brief      deadline_timer handler
 * Called when the timer expires and/or is cancelled
 *
 * @param[in]  e     Cause of handler call
 */
void timerHandler(const boost::system::error_code &e)
{
	if(e != 0)
	{
    	cout << "error code: " << e << " - " << e.message() << endl;
    	return;
    }
    cout << "Deadline timer!" << endl;
}


int main()
{
    // Declares required objects
    boost::asio::io_service ios;
    boost::asio::deadline_timer timer(ios);

    // Sets initial timer deadline. This will be canceled
    timer.expires_from_now(boost::posix_time::seconds(1));
    timer.async_wait(boost::bind(timerHandler, boost::asio::placeholders::error));

    // Runs io_service in a separate thread, in order to
    // continue main's execution
    boost::thread thr(boost::bind(&boost::asio::io_service::run, &ios));

    // Reschedules timer. First wait's handler will be called with 
    // error code boost::asio::operation_aborted
    timer.expires_from_now(boost::posix_time::seconds(5));
    timer.async_wait(boost::bind(timerHandler, boost::asio::placeholders::error));
    
    // Syncronization point: when ios goes out of work, the thread will join
    // allowing app to terminate.
    thr.join();
}
