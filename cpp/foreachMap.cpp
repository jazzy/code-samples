/*
* Sample application to demonstrate std::for_each usage
* on an std:map to update the map 's elements.
*
* Compile with
* $ g++ foreachMap.cpp -o foreachMap
*/

#include <map>
#include <algorithm>
#include <string>
#include <iostream>

#include <boost/bind.hpp>

using namespace std;

/**
 * Data structure with two fields. One is set while inserting
 * pairs into the map and the other is set when calling std::for_each.
 */
typedef struct MapElementsData
{
	string firstField;
	string secondField;
} MapElementsData_t;

/**
 * @brief      Class that holds all the map.
 */
class ForEachMap
{
public:
	ForEachMap() {};
	~ForEachMap() {};

	 /**
	  * @brief      Adds an element to the map
	  * The value 's firstField is set to sDescription and its
	  * secondField is set to an empty string
	  *
	  * @param[in]  index         Map index
	  * @param[in]  sDescription  String to be used in the firstField field
	  */
	void addMapElement(const int index, std::string sDescription)
	{
		MapElementsData_t element = {sDescription, ""};
		m_data.insert(std::pair<const int, MapElementsData_t>(index, element));
	}

	/**
	 * @brief      Calls the std::for_each algorithm to update all 
	 * values ' secondField
	 */
	void setSecondFields()
	{
		std::for_each(m_data.begin(), m_data.end(),
			boost::bind(&ForEachMap::operation, this, _1));
	};

	void operation(
		std::pair<const int, MapElementsData_t> &currentPair)
	{
		currentPair.second.secondField = string("Copy of ") + 
				currentPair.second.firstField;
	}

	map<const int, MapElementsData_t>::const_iterator begin()
	{
		return m_data.begin();
	}

	map<const int, MapElementsData_t>::const_iterator end()
	{
		return m_data.end();
	}
private:

	map<const int, MapElementsData_t> m_data;
};
int main()
{
	ForEachMap myMap;
	// Adds elements to map
	myMap.addMapElement(2, "Two");
	myMap.addMapElement(7, "Seven");
	myMap.addMapElement(4, "Four");
	myMap.addMapElement(1, "One");

	// Transforms elements
	myMap.setSecondFields();

	for(map<const int, MapElementsData_t>::const_iterator iter = myMap.begin();
		iter != myMap.end();
		iter++)
	{
		cout << iter->second.secondField << endl;
	}

	return 0;
}