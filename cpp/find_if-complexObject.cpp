/*
* Sample application to demonstrate std::find_if usage
* on a container of some complex structure objects.
* 
* It is defined a complex structure and the functor
* "Finder" that defines the criteria. In this example,
* the criteria is the integer value of the complex object
* to be 3.
*
* Compile with
* $ g++ find_if-complexObject.cpp -o find_if-complexObject
*/

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * @brief      Complex object
 * 
 * This class is the one 
 * For this particular example, there was no need to create a complex
 * object, however, the example is much more complete and more like
 * real-world situations.
 */
struct ComplexObject
{
    ComplexObject() : value(0) {};
    ComplexObject(int initValue) : value(initValue) {}; 
    
    int value;
};

/**
 * @brief      Functor to be used by find_if
 */
struct Finder
{
    /**
     * Predicate definition.
     * Parameter is each element of the container as a const reference.
     * Returns boolean (indicating if the predicate is true or false)
     */

    bool operator()(const ComplexObject &obj)
    {
        return obj.value==3;
    }
} finder;

int main()
{
    // Declares and populates the container
    vector<ComplexObject> vObjs;
    vObjs.push_back(ComplexObject(4));
    vObjs.push_back(ComplexObject(1));
    vObjs.push_back(ComplexObject(10));
    vObjs.push_back(ComplexObject(3));
    vObjs.push_back(ComplexObject(2));
    
    // Searches using predicate. Note that "finder" is a global variable
    vector<ComplexObject>::iterator iter = 
        find_if(vObjs.begin(), vObjs.end(), finder);
        
    // Prints result
    if(vObjs.end() == iter)
    {
        cout << "Object not found." << endl;
    }
    else
    {
        cout << "Object found at position " << distance(vObjs.begin(), iter) << endl;
    }
}
