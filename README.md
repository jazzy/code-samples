# Miscelaneous source examples
## C++
### STL
* [Using std::for_each on an std:map to change a map 's contents](cpp/foreachMap.cpp)
* [Using std::transform on an container to convert its contents](cpp/containerStructConverter.cpp)
* [Using find_if on a container of non-POD data](cpp/find_if-complexObject.cpp)

### [Boost](http://www.boost.org)
* [Changing deadline on a boost::asio:deadline_timer](cpp/deadline_timerTest.cpp)
